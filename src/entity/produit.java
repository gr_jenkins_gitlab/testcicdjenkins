 
public class produit {
    
   private int id ;
   private String nom;
   private Double prix;
public int getId() {
    return id;
}
public String getNom() {
    return nom;
}
public Double getPrix() {
    return prix;
}
public void setId(int id) {
    this.id = id;
}
public void setNom(String nom) {
    this.nom = nom;
}
public void setPrix(Double prix) {
    this.prix = prix;
}
@Override
public int hashCode() {
    final int prime = 38;
    int result = 1;
    result = prime * result + id;
    result = prime * result + ((nom == null) ? 0 : nom.hashCode());
    result = prime * result + ((prix == null) ? 0 : prix.hashCode());
    return result;
}
@Override
public boolean equals(Object obj) {
    if (this == obj)
        return true;
    if (obj == null)
        return false;
    if (getClass() != obj.getClass())
        return false;
    produit other = (produit) obj;
    if (id != other.id)
        return false;
    if (nom == null) {
        if (other.nom != null)
            return false;
    } else if (!nom.equals(other.nom))
        return false;
    if (prix == null) {
        if (other.prix != null)
            return false;
    } else if (!prix.equals(other.prix))
        return false;
    return true;
} 


}
